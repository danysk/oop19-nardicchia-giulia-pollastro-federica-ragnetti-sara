package model.minigames.perilouspath;

/**
 * Implementation of a simple {@link Mine}.
 */
public class SimpleMineImpl extends AbstractMine {

    /**
     * Constructor of a simple {@link Mine}.
     * 
     * @param size
     *          the upper bound's size
     */
    public SimpleMineImpl(final int size) {
        super(size);
    }
}
