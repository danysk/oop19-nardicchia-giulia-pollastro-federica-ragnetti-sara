package model.minigames.truecolor;

/**
 * Enumeration for the possible uses of color.
 *
 */
public enum StatusColor {

    /**
     * Value that identify the color to focus on meaning.
     */
    MEANCOLOR,

    /**
     * Value that identify the color to focus true color.
     */
    TRUECOLOR;

}
