PERILOUS PATH
At each level, you have to memorize the position of the mines having 5 seconds of time. The goal of the game is to find one of the possible paths from start to finish avoiding the mines, by clicking buttons.
 
TRUE COLOR
At each level, you have to click the "YES" button if the meaning of the button text with shadow corresponds to the true color of the other buttons text below.
 
ONE WAY
Starting from an initial position, you have to calculate the path, following the arrows shown in the buttons above. Once the final position is found, click on the corresponding button to win and start the next level.
 
SIZE COUNT
At each level, you have to choose the operation that gives the greatest result or the "same" button if the results are equals.
 
COLOR GAMA
At each level you have to find the only color suggested by the question which can be the lightest or the darkest among those presented

